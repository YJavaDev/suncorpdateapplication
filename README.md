"# My project's README" 
Suncorp Date Duration Calculator
1.	There are two projects, SuncorpDateCalculator for standalone application and SuncorpRestService for Rest API which is Dynamic Web Project.
Note : All these projects are build with JDK 1.8, Tomcat 8 and Eclipse Mars 
2.	SuncorpDateCalculator project setup guideline
 
 
 
Add JDK 1.8 in configure and build path.
 
Then add Tomcat 8 as per below screenshot with jdk1.8 only.
 
 
3.	SuncorpRestService Project Setup
 

Add JDK 8 and clean Build with Tomcat.
 
How to execute and test SuncorpDateCalculator?
SuncorpDateCalculator-> DateDurationCalculator main class from Command Line.
DateUnitTest.java JUnit test class with almost all test cases.

How to test SuncorpRestService?
There are two ways to test rest 
1.	 TestDateRestService.java and 
2.	Chrome Postman app.
Use same test cases as mentioned in DateUnitTest.java in TestDateRestService.java or Postman params.
Install Postman from below URL – or Click Here
https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en
Postman screenshot- 
http://localhost:8080/SuncorpRestService/rest/date/getDatesDiff?reqParam=01 02 2001,01 03 2005

git clone https://YJavaDev@bitbucket.org/YJavaDev/suncorpdateapplication.git
 



