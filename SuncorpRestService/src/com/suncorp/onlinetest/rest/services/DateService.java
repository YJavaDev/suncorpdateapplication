package com.suncorp.onlinetest.rest.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.suncorp.onlinetest.date.exception.ValidatorException;
import com.suncorp.onlinetest.date.main.DateBeanHelper;
import com.suncorp.onlinetest.date.util.DateValidator;

@Path("/date")
public class DateService {

	final static Logger logger = Logger.getLogger(DateValidator.class);

	@GET
	@Path("/getDatesDiff")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getDatesDuration(@QueryParam("reqParam") String inputReq) {
		logger.warn("Executing operation DateService.getDatesDuration() with inputs :]"+inputReq+"[");
		DateBeanHelper dateController = new DateBeanHelper();
		Integer result = null;
		
		try {
			result = dateController.getDaysBetweenDates(inputReq);
		} catch (ValidatorException e) {
			// TODO Auto-generated catch block
			logger.warn(e.getErrorCode()+":"+e.getMessage());
			return Response.status(Integer.parseInt(e.getErrorCode())).entity(e.getMessage()).build();
		}
		
		logger.warn("Exiting operation DateService.getDatesDuration() with result :]"+result+"[");
		return Response.status(201).entity(result.toString()).build();
	}
	
	private String getDate(String date){
		return date;
	}


	// This method is called if XML is request
	@GET
	@Produces(MediaType.TEXT_XML)
	public String sayXMLHello() {
		return "<?xml version=\"1.0\"?>" + "<hello> Date Duration Calculator" + "</hello>";
	}

	// This method is called if HTML is request
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String sayHtmlHello() {
		return "<html> " + "<title>" + "Date Duration Calculator" + "</title>" + "<body><h1>" + "Date Duration Calculator" + "</body></h1>"
				+ "</html> ";
	}

}
