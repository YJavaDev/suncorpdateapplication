package com.suncorp.online.test;

	import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

	public class TestDateRestService {

	        public static void main(String[] args) {
	                ClientConfig config = new ClientConfig();

	                Client client = ClientBuilder.newClient(config);

	                WebTarget target = client.target(getBaseURI());

	                String reqParam="01 02 2001,01 03 2005";
	                Response response =
	                                target.path("rest").path("date/getDatesDiff").queryParam("reqParam", reqParam).request().accept(MediaType.TEXT_PLAIN).get();
	                System.out.println("Received Reponse Code : "+response.getStatus()+" with Response :"+ response.readEntity(String.class));
	                
	        }

	        private static URI getBaseURI() {
	                return UriBuilder.fromUri("http://localhost:8080/SuncorpRestService").build();
	        }
	}
