package com.suncorp.onlinetest.junit.date;

import static org.junit.Assert.fail;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.sun.net.httpserver.Authenticator.Success;
import com.suncorp.onlinetest.date.exception.ValidatorException;
import com.suncorp.onlinetest.date.main.DateBeanHelper;
import com.suncorp.onlinetest.date.main.IDateBeanHelper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DateUnitTest {

	IDateBeanHelper dateBean = new DateBeanHelper();
	@Rule
	  public ExpectedException exception = ExpectedException.none();
	
	
	/**Success Test Case to check difference between date is 18
	 * @throws Exception
	 */
	@Test
	public void testValidatorException() throws Exception {
		Integer result = dateBean.getDaysBetweenDates("01 02 2010,19 02 2010");
		assertThat(result.toString(),is("18"));
	}
	
	/**
	 * Test Invalid Characters in Input
	 */
	@Test
	public void testInvalidCharacters() {
		try {
			
		    dateBean.getDaysBetweenDates("01, 02 2010,02 02 2016");
		    fail();
		} catch (ValidatorException e) {
			assertThat(e.getErrorCode(),is("2001"));
		} catch (Exception e) {
			
		}
	}
	
	
	
	/**
	 * Test First Date Larger than Second
	 */
	@Test
	public void testFirstDateLargerThanSecond() {
		try {
			
		    dateBean.getDaysBetweenDates("01 12 2010,02 02 2000");
		    fail();
		} catch (ValidatorException e) {
			assertThat(e.getErrorCode(),is("2003"));
		} catch (Exception e) {
			
		}
	}
	
	/**
	 * To test Empty Input
	 */
	@Test
	public void testEmptyOrNullInput() {
		try {
			
		    dateBean.getDaysBetweenDates("");
		    fail();
		} catch (ValidatorException e) {
			assertThat(e.getErrorCode(),is("2004"));
		} catch (Exception e) {
			
		}
	}
	
	/**
	 * To test Invalid characters
	 */
	@Test
	public void testInvalidFormatInput() {
		try {
			
		    dateBean.getDaysBetweenDates("01 ab 2010,02 02 2016");
		    fail();
		} catch (ValidatorException e) {
			assertThat(e.getErrorCode(),is("2001"));
		} catch (Exception e) {
			
		}
	}
	
	/**
	 * Date Out Of allowed range i.e. 1900-2010
	 */
	@Test
	public void testDateGreaterThanAllowedDate() {
		try {
			
		    dateBean.getDaysBetweenDates("01 02 2010,02 02 2016");
		    fail();
		} catch (ValidatorException e) {
			assertThat(e.getErrorCode(),is("1003"));
		} catch (Exception e) {
			
		}
	}
	
	/**
	 * Non leap Year Invalid date i.e. greater than 28 for non leap year
	 */
	@Test
	public void InvalidDateForNonLeapYear() {
		try {
			
		    dateBean.getDaysBetweenDates("29 02 2001,01 04 2010");
		    fail();
		} catch (ValidatorException e) {
			assertThat(e.getErrorCode(),is("1004"));
		} catch (Exception e) {
			
		}
	}
	
	/**
	 * Leap Year Invalid date i.e. greater than 28 for non leap year
	 */
	@Test
	public void InvalidDateForLeapYear() {
		try {
			
		    dateBean.getDaysBetweenDates("30 02 2000,01 04 2010");
		    fail();
		} catch (ValidatorException e) {
			assertThat(e.getErrorCode(),is("1005"));
		} catch (Exception e) {
			
		}
	}
	
	/**
	 * Invalid/greater date than that month's max allowed days
	 * for eg. 31 in Apr
	 */
	@Test
	public void InvalidDateForMonth() {
		try {
			
		    dateBean.getDaysBetweenDates("31 04 2000,01 04 2010");
		    fail();
		} catch (ValidatorException e) {
			assertThat(e.getErrorCode(),is("1006"));
		} catch (Exception e) {
			
		}
	}
}
