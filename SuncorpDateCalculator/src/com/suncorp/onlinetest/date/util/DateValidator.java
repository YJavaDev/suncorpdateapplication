package com.suncorp.onlinetest.date.util;

import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.suncorp.onlinetest.date.exception.ValidatorException;

public class DateValidator implements IValidator {
	
	final static Logger logger = Logger.getLogger(DateValidator.class);
	
	/* Input Validator - Pattern of Input(mentioned in AppConstants.dateRegex),
	 * All Numerics, Format, Date Range, Dates not more than max days in that month
	 * 29Feb of non-Lip year not allowed
	 * 
	 * @see com.suncorp.onlinetest.date.util.IValidator#validate(java.lang.String)
	 */
	@Override
	public void validate(String chkDate) throws ValidatorException {
		logger.warn("Entering DateValidator.validating : input :]"+chkDate+"[");
		
		if(StringTool.isNullOrEmpty(chkDate))
			throw new ValidatorException("1001",AppUtil.loadProperties().getString("1001"));
		if(!Pattern.matches(AppConstants.dateRegex, chkDate)){
			throw new ValidatorException("1002",AppUtil.loadProperties().getString("1002"));
		}
		int[] dateArray;
		try {
			 dateArray = Arrays.stream(chkDate.split(AppConstants.dateSeperator))
		            .map(String::trim).mapToInt(Integer::parseInt).toArray();
			 //dateArray = chkDate.split(DateConstants.dateSeperator);
			            
		}
		catch (NumberFormatException npe){
			throw new ValidatorException("1002",AppUtil.loadProperties().getString("1002"));
		}
		if(!DateUtil.isLeapYear(dateArray[2]) && dateArray[1]==2 && dateArray[0]>28){
			throw new ValidatorException("1004",AppUtil.loadProperties().getString("1004"));
		}
		else if (DateUtil.isLeapYear(dateArray[2]) && dateArray[1]==2 && dateArray[0]>29){
			throw new ValidatorException("1005",AppUtil.loadProperties().getString("1005"));
		}
		if(Months.getDaysInMonth(dateArray[1], dateArray[2])==0 ||
				dateArray[0]>Months.getDaysInMonth(dateArray[1], dateArray[2])){
			throw new ValidatorException("1006",AppUtil.loadProperties().getString("1006"));
		}
		if(dateArray[2]<AppConstants.allowedFromYear||dateArray[2]>AppConstants.allowedToYear){
			throw new ValidatorException("1003",AppUtil.loadProperties().getString("1003"));
		}
		
		
		logger.warn("Exiting DateValidator.validating : input :]"+chkDate+"[");
	}
	
}
