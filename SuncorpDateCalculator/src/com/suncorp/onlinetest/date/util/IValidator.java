package com.suncorp.onlinetest.date.util;

import com.suncorp.onlinetest.date.exception.ValidatorException;

public interface IValidator {
	public void validate(String date) throws ValidatorException;

}
