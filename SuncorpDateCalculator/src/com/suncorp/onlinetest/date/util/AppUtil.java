package com.suncorp.onlinetest.date.util;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class AppUtil {
	final static Logger logger = Logger.getLogger(AppUtil.class);
	
	/**Load properties from properties file
	 * @return
	 */
	public static ResourceBundle loadProperties(){
		logger.warn("Loading Application properties...");
		ResourceBundle rb = null;

		try {

    		rb = ResourceBundle.getBundle ("com.suncorp.onlinetest.date.util.application");
    		
		} catch (Exception e) {
			logger.warn("Error retrieving properties file: " + e);
		}
		
		return rb;
	}
}
