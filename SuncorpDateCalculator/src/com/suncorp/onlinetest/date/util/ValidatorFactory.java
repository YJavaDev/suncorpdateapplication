package com.suncorp.onlinetest.date.util;

public class ValidatorFactory {

	public static IValidator getValidator(String validatorType){
		
		IValidator validator = null;;
		if(validatorType.equals("inputValidator"))
			validator =  new InputValidator();
		else if(validatorType.equals("dateValidator"))
			validator =  new DateValidator();
		
		return validator;
	}
}
