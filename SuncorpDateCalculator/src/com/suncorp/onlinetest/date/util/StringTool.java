package com.suncorp.onlinetest.date.util;

public class StringTool {

	public static boolean isNullOrEmpty(String str){
		boolean isNullOrEmpty = false;
		
		if(str==null||str.isEmpty()||str.trim().equals(""))
			isNullOrEmpty=true;
			
		return isNullOrEmpty;		
	}
}
