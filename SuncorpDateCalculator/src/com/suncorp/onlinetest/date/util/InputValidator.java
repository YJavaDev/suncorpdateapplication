package com.suncorp.onlinetest.date.util;

import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.suncorp.onlinetest.date.exception.ValidatorException;

public class InputValidator implements IValidator {
	final static Logger logger = Logger.getLogger(InputValidator.class);
	
	@Override
	public void validate(String input) throws ValidatorException {
		
		logger.warn("Entering InputValidator.validating : input :]"+input+"[");
		
		if(StringTool.isNullOrEmpty(input)){
			throw new ValidatorException("2004",AppUtil.loadProperties().getString("2004"));
		}
		else if(!Pattern.matches(AppConstants.inputStrictRegex, input)){
			throw new ValidatorException("2001",AppUtil.loadProperties().getString("2001"));
		}
		
		String fromDate =input.split(",")[0];
		String toDate =input.split(",")[1];
		
		int[] firstDate = Arrays.stream(fromDate.split(AppConstants.dateSeperator))
	            .map(String::trim).mapToInt(Integer::parseInt).toArray();
		int[] secDate  = Arrays.stream(toDate.split(AppConstants.dateSeperator))
	            .map(String::trim).mapToInt(Integer::parseInt).toArray();
		
		if(fromDate.trim().split(" ").length>3 || toDate.trim().split(" ").length>3 ){
			throw new ValidatorException("2002",AppUtil.loadProperties().getString("2002"));
		}
		 else if (firstDate[2] > secDate[2] || (firstDate[2] == secDate[2] && firstDate[1] > secDate[1])
						|| (firstDate[2] == secDate[2] && firstDate[1] == secDate[1]) && firstDate[0] > secDate[0]) {
			 throw new ValidatorException("2003",AppUtil.loadProperties().getString("2003"));
		}
		
		logger.warn("Exiting InputValidator.validating : input :]"+input+"[");
	}
	
}
