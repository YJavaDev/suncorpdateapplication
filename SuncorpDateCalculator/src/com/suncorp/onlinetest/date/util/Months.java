package com.suncorp.onlinetest.date.util;

import java.util.HashMap;
import java.util.Map;

public enum Months {

	/*
	 * JAN(1,"JAN"), FEB(2), MAR(3), APRIL(4), MAY(5), JUN(6), JUL(7), AUG(8),
	 * SEP(9), OCT( 10), NOV(11), DEC(12);
	 */
	JAN(1, "31"), FEB(2, "28"), MAR(3, "31"), APRIL(4, "30"), MAY(5, "31"), JUN(6, "30"), JUL(7, "31"), AUG(8,
			"31"), SEP(9, "30"), OCT(10, "31"), NOV(11, "30"), DEC(12, "31");
	private final Integer id;
	private final String days;

	Months(Integer id, String days) {
		this.id = id;
		this.days = days;
	}

	private static final Map<Integer, String> monthValues = new HashMap<Integer, String>();

	static {
		for (Months month : Months.values()) {
			monthValues.put(month.id, month.days);
		}
	}

	public static Integer getDaysInMonth(Integer month, Integer year) {
		Integer days = null;
			if (DateUtil.isLeapYear(year)) {
				monthValues.put(2, "29");
			} else {
				monthValues.put(2, "28");
			}
			days = Integer.parseInt(monthValues.get(month)!=null?monthValues.get(month):"0");
			// return DateUtil.isLeapYear(year) ?
			// Integer.parseInt(monthValues.get(month))+1:Integer.parseInt(monthValues.get(month));
		return days;
	}
	
	public static Map<Integer, String> geMonthsData(Integer year) {
		
		if(DateUtil.isLeapYear(year)){
			monthValues.put(2, "29");
		}
		else{
			monthValues.put(2, "28");
		}
		return monthValues;
	}

}
