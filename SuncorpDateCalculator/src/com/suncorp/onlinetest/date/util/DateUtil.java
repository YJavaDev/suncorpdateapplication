package com.suncorp.onlinetest.date.util;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

public class DateUtil {
	
	final static Logger logger = Logger.getLogger(DateUtil.class);
	
	/**
	 * This core method actually calculates Duration between two dates
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public static Integer getDatesDiffernce(String fromDate, String toDate) {
		
		logger.warn("Entering DateUtil.getDatesDiffernce for :"+fromDate+" AND "+toDate);
		
		int[] firstDate = Arrays.stream(fromDate.split(AppConstants.dateSeperator))
	            .map(String::trim).mapToInt(Integer::parseInt).toArray();
		int[] secDate = Arrays.stream(toDate.split(AppConstants.dateSeperator))
	            .map(String::trim).mapToInt(Integer::parseInt).toArray();
		
		
		

		Integer leftDays = new DateUtil().calculateDaysFromMonth(firstDate[1], firstDate[2], true) - (firstDate[0]);

		Integer passedDays = new DateUtil().calculateDaysFromMonth(secDate[1], secDate[2], false)
				- (Months.getDaysInMonth(secDate[1], secDate[2]) - (secDate[0]));
		
		Integer finalRes = null;
		
		if(firstDate[2]==(secDate[2]))
			//finalRes = leftDays + passedDays- daysInAllYrs;
			finalRes = passedDays - (Months.getDaysInMonth(firstDate[1]-1, firstDate[2]) +firstDate[0]) ;
		else if((firstDate[2]+1)==(secDate[2])){
			finalRes = leftDays + passedDays ;
		}
		else if((secDate[2]-firstDate[2])>=2){
			Integer daysInAllYrs = new DateUtil().calculateDaysInYears(firstDate[2]+1, secDate[2]-1);
			finalRes = leftDays + passedDays +daysInAllYrs ;
		}
		logger.warn("Exiting DateUtil.getDatesDiffernce with Result :"+finalRes);

		return finalRes;
	}
	
	
	//TODO change to private after testing
	
	/** This method is used only when years field difference is more than 1 for eg. 2001 & 2003+
	 *	Otherwise days can be calculated with other method (YTD)
	 * @param years
	 * @return
	 */
	private  Integer calculateDaysInYears(Integer... years) {
		// TODO Auto-generated method stub
		int daysInYears=0;
		
		for (int i=years[0];i<=years[1];i++){
			daysInYears+=(isLeapYear(i)?366:365);
		}
		
		return daysInYears;
	}
	
	/**
	 * Calculate days left in that from provided month and yar inputs
	 * For eg. 12 2016 - return 31 days left in year till end of year
	 * 		   01 2016 - returns 366 days from Jan till end of year 	 * 			
	 * 
	 * @param month
	 * @param year
	 * @param lesser
	 * @return
	 */
	private Integer calculateDaysFromMonth(Integer month, Integer year, boolean lesser){
		
		int days=0;
		
		if(lesser)
			days = Months.geMonthsData(year).entrySet().stream()
			        .filter(map -> map.getKey().intValue()>=month)
			        .map(map -> map.getValue())
			        .collect(Collectors.toList()).stream().mapToInt(Integer::valueOf).sum();
		else
			days = Months.geMonthsData(year).entrySet().stream()
			        .filter(map -> map.getKey().intValue()<=month)
			        .map(map -> map.getValue())
			        .collect(Collectors.toList()).stream().mapToInt(Integer::valueOf).sum();
		
		//return DateUtil.isLeapYear(year) ? days+1:days;
		return days;
		
	}
	
	/**
	 * Check if the year is Leap or not
	 * @param year
	 * @return
	 */
	public static boolean isLeapYear(Integer year){
	
		return (year%4==0?true:false);
		
	}
	
}
