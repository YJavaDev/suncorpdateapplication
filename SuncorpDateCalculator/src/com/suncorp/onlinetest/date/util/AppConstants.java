package com.suncorp.onlinetest.date.util;

public class AppConstants {

	public final static String dateSeperator = "\\s+";
	public final static String dateRegex = "^[0-9 \\t]+$";

	public final static String inputRegexWithFewSpaces = "^[\\d ]+[\\,][\\d ]+$";
	public final static String inputStrictRegex ="^[\\d][\\d][ ][\\d][\\d][ ][\\d][\\d][\\d][\\d][\\,][\\d][\\d][ ][\\d][\\d][ ][\\d][\\d][\\d][\\d]+$";
	
	public final static Integer allowedFromYear = 1900;
	public final static Integer allowedToYear = 2010;
}
