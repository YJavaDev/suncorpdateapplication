package com.suncorp.onlinetest.date.main;

public interface IDateBeanHelper {
	
	public Integer getDaysBetweenDates(String inputReq) throws Exception;
}
