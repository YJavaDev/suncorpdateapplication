package com.suncorp.onlinetest.date.main;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class DateDurationCalculator {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {

		Scanner scanner = null;
		// String[] vars = new String[2];
		String fileName = null;
		String inputDate = null;
		while (true) {

			try {

				scanner = new Scanner(System.in);
				System.out.println("***************Welcome to Date Duration Calculator***************");
				System.out.println("Do you want to read inputs from File or Command Line Interface? \nPress 1 For Input from Command Prompt Or any key to provide full file path\n");
				System.out.println("OR Enter 'q' to exit\n");
				String inputChoice = scanner.next();

				if (inputChoice.equals("q")) {
					System.out.println("System Exiting...!\nGood Bye!");
					break;
				}

				if (inputChoice.equals("1")) {
					System.out.println("Please enter the dates to find difference in DD MM YYYY,DD MM YYYY Format./n");
					scanner = new Scanner(System.in);
					inputDate = scanner.nextLine();
					System.out.println("Calculating duration for dates... : "+inputDate);
					
					
					System.out.println("****Result is :"+new DateBeanHelper().getDaysBetweenDates(inputDate));
					System.out.println("Please continue with new inputs...\n");
					// scanner.close();
				} else {
					System.out.println("Please enter full file path to read input from : \n");
					scanner = new Scanner(System.in);
					fileName = scanner.nextLine();
					scanner = new Scanner(new File(fileName));
					while (scanner.hasNext()) {
						inputDate = scanner.nextLine();
						System.out.println("Calculating duration for dates... : "+inputDate);
						System.out.println("****Result is :"+new DateBeanHelper().getDaysBetweenDates(inputDate));
						System.out.println("Please continue with new inputs...\n");
					}

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
				System.out.println("***********Please try again with new inputs*********");
			} finally {
				
			}
		}
		scanner.close();
		// String fileName =
		// "E://Yogesh//CarSimulator//SuncorpDateCalculator//JUnit//com//suncorp//onlinetest//junit//date//TestData.txt";

	}

}
