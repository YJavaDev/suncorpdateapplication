package com.suncorp.onlinetest.date.main;

import org.apache.log4j.Logger;

import com.suncorp.onlinetest.date.exception.ValidatorException;
import com.suncorp.onlinetest.date.util.DateUtil;
import com.suncorp.onlinetest.date.util.IValidator;
import com.suncorp.onlinetest.date.util.ValidatorFactory;

public class DateBeanHelper implements IDateBeanHelper {

	final static Logger logger = Logger.getLogger(DateBeanHelper.class);

	IValidator validator = null;

	/* This is helper method to calculate the duration between two dates 
	 * without using any java/third party library
	 * 
	 * @input - Comma separated Dates
	 * @output - Difference between those two
	 * @see com.suncorp.onlinetest.date.helper.IDateBeanHelper#getDaysBetweenDates(java.lang.String)
	 */
	@Override
	public Integer getDaysBetweenDates(String inputReq) throws ValidatorException {
		logger.info("Entering Method DateBeanHelper.getDaysBetweenDates() inputs :]" + inputReq + "[");
		Integer diffInDays = null;
		try {

			validateInput(inputReq);
			
			String fromDate = inputReq.split(",")[0];
			String toDate = inputReq.split(",")[1];
			
			diffInDays = DateUtil.getDatesDiffernce(fromDate, toDate);
		} catch (ValidatorException ve) {
			logger.warn(ve.getErrorCode() + ":" + ve.getMessage());
			throw ve;
		} catch (Throwable e) {
			logger.warn(e.getMessage());
			throw e;
		}
		logger.info("Exiting Method DateBeanHelper.getDaysBetweenDates() Result :]" + diffInDays + "[");
		// Exception Handling
		return diffInDays;
	}

	private void validateInput(String inputReq) throws ValidatorException {
		validator = ValidatorFactory.getValidator("inputValidator");
		validator.validate(inputReq);

		String fromDate = inputReq.split(",")[0];
		String toDate = inputReq.split(",")[1];

		validator = ValidatorFactory.getValidator("dateValidator");
		validator.validate(fromDate);
		validator.validate(toDate);
		
	}

}
