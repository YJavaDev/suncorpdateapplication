package com.suncorp.onlinetest.date.exception;

public class ValidatorException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5314641504021536706L;
	
	private String errorCode="Unknown_Exception";
	
	public ValidatorException(String eMsg){
		super(eMsg);
	}
	
	public ValidatorException(String errorCode,String message){
		super(message);
		this.errorCode=errorCode;
	}
	
	public String getErrorCode(){
		return this.errorCode;
	}
}
